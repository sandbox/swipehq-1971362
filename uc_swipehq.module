<?php

/**
 * @file
 * Integrates Swipe's payment service
 */

$GLOBALS['uc_swipehq_debug']        = variable_get('uc_swipehq_debug', '0');
$GLOBALS['uc_swipehq_test_mode']    = variable_get('uc_swipehq_test_mode', '1');

if(!defined('SWIPE_WATCHDOG_NAME')) define('SWIPE_WATCHDOG_NAME', 'Swipe Checkout');



/**
 * Implements hook_help().
 */
function uc_swipehq_help($path, $arg) {
  switch ($path) {
    case 'admin/store/settings/payment/method/%':
      if ($arg[5] == 'swipehq') {
        return '<p>' . t('To use this payment method you need to become a <a href="https://www.swipehq.com/support/support_detail.php?cat=General+Questions&title=Becoming+a+Swipe+HQ+Checkout+merchant">Swipe Checkout merchant</a>') . '</p>';
      }
  }
}

/**
 * Implements hook_menu().
 */
function uc_swipehq_menu() {
  $items = array();

  $items['cart/swipehq/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_swipehq_complete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_swipehq.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uc_swipehq_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_swipehq_method_title';
  $conf['i18n_variables'][] = 'uc_swipehq_checkout_button';
}

/**
 * Implements hook_ucga_display().
 */
function uc_swipehq_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'swipehq' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implements hook_uc_payment_method().
 *
 * @see uc_payment_method_swipehq()
 */
function uc_swipehq_uc_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_swipehq');
  $title = variable_get('uc_swipehq_method_title', t('Credit card on a secure server:'));
  $title .= '<br />' . theme('image', array(
    'path' => drupal_get_path('module', 'uc_swipehq') . '/swipe-checkout-logo.png',
    'attributes' => array('class' => array('uc-swipehq-logo')),
  ));

  $methods['swipehq'] = array(
    'name' => t('Swipe Checkout'),
    'title' => $title,
    'review' => t('Credit card'),
    'desc' => t('Redirect to Swipe Checkout to pay by credit card.'),
    'callback' => 'uc_payment_method_swipehq',
    'redirect' => 'uc_swipehq_form',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Adds Swipe Checkout settings to the payment method settings form.
 *
 * @see uc_swipehq_uc_payment_method()
 */
function uc_payment_method_swipehq($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'settings':
        $statuses_data = uc_order_status_list();
        $statuses = array();
        foreach ($statuses_data as $status) {
            $statuses[$status['id']] = $status['title'];
        }
        $default_status = $statuses_data[0]['id'];
        
        //API credentials
        $form['uc_swipehq_merchant'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('Merchant ID'),
          '#description' => t('Find this in your Swipe Merchant login under Settings -> API Credentials'),
          '#default_value' => variable_get('uc_swipehq_merchant', '')
        );
        $form['uc_swipehq_api_key'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('API key'),
          '#description' => t('Find this in your Swipe Merchant login under Settings -> API Credentials'),
          '#default_value' => variable_get('uc_swipehq_api_key', '')
        );
        $form['uc_swipehq_api_url'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('API Url'),
          '#description' => t('Find this in your Swipe Merchant login under Settings -> API Credentials'),
          '#default_value' => variable_get('uc_swipehq_api_url', '')
        );        
        $form['uc_swipehq_payment_page_url'] = array(
          '#type' => 'textfield',
          '#required' => TRUE,
          '#title' => t('Payment Page Url'),
          '#description' => t('Find this in your Swipe Merchant login under Settings -> API Credentials'),
          '#default_value' => variable_get('uc_swipehq_payment_page_url', '')
        );
                

        $form['uc_swipehq_paid_status'] = array(
          '#type' => 'select',
          '#title' => t('Paid order status'),
          '#options' => $statuses,
          '#default_value' => variable_get('uc_swipehq_paid_status', isset($statuses['payment_received']) ? 'payment_received' : $default_status)
        );
        $form['uc_swipehq_failed_status'] = array(
          '#type' => 'select',
          '#title' => t('Failed payment order status'),
          '#options' => $statuses,
          '#default_value' => variable_get('uc_swipehq_failed_status', isset($statuses['pending']) ? 'pending' : $default_status)
        );
      
        $form['uc_swipehq_test_mode'] = array(
          '#type' => 'checkbox',
          '#title' => t('Test mode'),
          '#description' => t('Please enable if your merchant account is in testing mode.'),
          '#default_value' => variable_get('uc_swipehq_test_mode', TRUE)
        );
        $form['uc_swipehq_test_paid_status'] = array(
          '#type' => 'select',
          '#title' => t('Test paid order status'),
            '#description' => t('Paid order status in testing mode'),
          '#options' => $statuses,
          '#default_value' => variable_get('uc_swipehq_test_paid_status', isset($statuses['processing']) ? 'processing' : $default_status)
        );
        $form['uc_swipehq_test_failed_status'] = array(
          '#type' => 'select',
          '#title' => t('Test failed payment order status'),
            '#description' => t('Failed payment order status in testing mode'),
          '#options' => $statuses,
          '#default_value' => variable_get('uc_swipehq_test_failed_status', isset($statuses['processing']) ? 'processing' : $default_status)
        );
        
        $form['uc_swipehq_method_title'] = array(
          '#type' => 'textfield',
          '#title' => t('Payment method title'),
          '#default_value' => variable_get('uc_swipehq_method_title', t('Credit card on a secure server:')),
        );
        $form['uc_swipehq_checkout_button'] = array(
          '#type' => 'textfield',
          '#title' => t('Order review submit button text'),
          '#description' => t('Provide SwipeHQ Checkout specific text for the submit button on the order review page.'),
          '#default_value' => variable_get('uc_swipehq_checkout_button', t('Submit Order'))
        );
        $form['uc_swipehq_success_text'] = array(
          '#type' => 'textarea',
          '#title' => t('Order complete text'),
          '#description' => t('Provide specific text for the succesfully completed order.'),
          '#default_value' => variable_get('uc_swipehq_success_text', t('Thank you for your order.'))
        );
        $form['uc_swipehq_debug'] = array(
          '#type' => 'checkbox',
          '#title' => t('Debug mode'),
          '#description' => t('Show debug info in the logs for payment transactions.'),
          '#default_value' => variable_get('uc_swipehq_test_mode', FALSE)
        );
      return $form;
  }
}

/**
 * Form to build the redirect to the payment page
 */
function uc_swipehq_form($form, &$form_state, $order) {
    global $uc_swipehq_debug, $uc_swipehq_test_mode;
    
    if ($uc_swipehq_test_mode) {
        drupal_set_message('Swipe Checkout: test mode enabled.', 'warning');
    }
    
    $data = array();
    
    $merchant = variable_get('uc_swipehq_merchant', '');
    $api_key = variable_get('uc_swipehq_api_key', '');
    $api_url = trim(variable_get('uc_swipehq_api_url', ''), '/');
    $payment_page_url = trim(variable_get('uc_swipehq_payment_page_url', ''), '/');
    
    // accepted currencies   
    $acceptedCurrenciesParams = array('merchant_id' => $merchant, 'api_key' => $api_key);
    $acceptedCurrenciesResponse = json_decode(_uc_swipehq_do_post_request($api_url.'/fetchCurrencyCodes.php', $acceptedCurrenciesParams), true);
    $acceptedCurrencies = $acceptedCurrenciesResponse['data'];
    
    $currency = $order->currency;
    
    if ($currency && in_array($currency, $acceptedCurrencies)) {
        
        //get list of ordered products
        $products = '';
        $i = 1;
        $count = count($order->products);
        foreach ($order->products as $product) {
            $products .= $product->qty . ' x ' . $product->title . ($i != $count ? '<br />' : '');
            $i++;
        }
        
        $amount = uc_currency_format($order->order_total, FALSE, FALSE, '.');
        
        if (!empty($merchant) && !empty($api_key)) {
            //get product ID using TransactionIdentifier API
            $params = array (
                'merchant_id'           => $merchant,
                'api_key'               => $api_key,
                'td_item'               => 'Order ID' . $order->order_id,
                'td_description'        => $products,
                'td_amount'             => $amount,
                'td_currency'           => $currency,
                'td_default_quantity'   => 1,
                'td_user_data'          => $order->order_id
            );

            if ($uc_swipehq_debug) {
                watchdog(SWIPE_WATCHDOG_NAME, 'Parameters for TransactionIdentifier API prepared: ' . json_encode($params) . ' (order ID' . $order->order_id . ')', array(), WATCHDOG_DEBUG);
            }

            $response = _uc_swipehq_do_post_request($api_url.'/createTransactionIdentifier.php', $params);
            if ($response) {
                
                if ($uc_swipehq_debug) {
                    watchdog(SWIPE_WATCHDOG_NAME, 'Response from TransactionIdentifier API: ' . $response . ' (order ID' . $order->order_id . ')', array(), WATCHDOG_DEBUG);
                }

                $response_data = json_decode($response);
                if ($response_data->response_code == 200 && !empty($response_data->data->identifier)) {
                    $form['#action'] = $payment_page_url;
                    $form['identifier_id'] = array('#type' => 'hidden', '#value' => $response_data->data->identifier);
                    $form['checkout'] = array('#type' => 'hidden', '#value' => 'true');
                    $form['shoppingcart'] = array('#type' => 'hidden', '#value' => 'Drupal Ubercart 1.0.0b');
                    
                    $form['actions'] = array('#type' => 'actions');
                    $form['actions']['submit'] = array(
                      '#type' => 'submit',
                      '#value' => variable_get('uc_swipehq_checkout_button', t('Submit Order')),
                    );

                    return $form;
                } else {
                    watchdog(SWIPE_WATCHDOG_NAME, 'Wrong API call status, response code: ' . $response_data->response_code . ' (order ID' . $order->order_id . ')', array(), WATCHDOG_ERROR);
                }
            }

        } else {
            watchdog(SWIPE_WATCHDOG_NAME, 'Undefined Merchant ID or API Key (order ID' . $order->order_id . ')', array(), WATCHDOG_CRITICAL);
        }
            
    }else{
    	watchdog(SWIPE_WATCHDOG_NAME, 'Attempted to pay for an item with a currency Swipe does not accept: '.$currency.'. Swipe accepts: '.join(', ', $acceptedCurrencies).'.', array(), WATCHDOG_CRITICAL);
    }

    drupal_set_message('Swipe does not support currency: '.$currency.'. Swipe supports: '.join(', ', $acceptedCurrencies).'.', 'error');
    return false;
}

/**
 * Transaction verification
 *
 * @param string $transaction_id
 * @return boolean
 */
function _uc_swipehq_verify_transaction($transaction_id) {
    global $uc_swipehq_debug;
    
    //parameters for transaction verification
    $data = array (
        'merchant_id'       => variable_get('uc_swipehq_merchant', ''),
        'api_key'           => variable_get('uc_swipehq_api_key', ''),
        'transaction_id'    => $transaction_id
    );

    if ($uc_swipehq_debug) {
        watchdog(SWIPE_WATCHDOG_NAME, 'Parameters for transaction verification prepared: ' . json_encode($data), array(), WATCHDOG_DEBUG);
    }

	$api_url = trim(variable_get('uc_swipehq_api_key', ''), '/');

    $response = _uc_swipehq_do_post_request($api_url.'/verifyTransaction.php', $data);
    if (!empty($response)) {
        if ($uc_swipehq_debug) {
            watchdog(SWIPE_WATCHDOG_NAME, 'Response from VerifyTransaction API: %$response.', array('%$response'=>$response), WATCHDOG_DEBUG);
        }

        $response_data = json_decode($response);

        if ($response_data->response_code == 200 && $response_data->data->transaction_id == $transaction_id) {
            return true;
        }
    }
    return false;
}

/**
 * Helper function to obtain Swipe Checkout URL for a transaction.
 */
function _uc_swipehq_do_post_request($url, $data) {
    $ch = curl_init ($url);
    curl_setopt ($ch, CURLOPT_POST, 1);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    $response = curl_exec ($ch);
    curl_close ($ch);

    if (!$response) {
        watchdog(SWIPE_WATCHDOG_NAME, 'CURL request error: ' . curl_error($ch) . '(' . curl_errno($ch) . ')', array(), WATCHDOG_ERROR);
    }

    return $response;
}
