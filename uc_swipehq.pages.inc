<?php

/**
 * @file
 * swipehq menu items.
 */


if(!defined('SWIPE_WATCHDOG_NAME')) define('SWIPE_WATCHDOG_NAME', 'Swipe Checkout');

/**
 * Finalizes swipehq transaction.
 */
function uc_swipehq_complete() {
    global $uc_swipehq_debug, $uc_swipehq_test_mode;
    
    if ($uc_swipehq_debug) {
        watchdog(SWIPE_WATCHDOG_NAME, 'Callback page has been requested with parameters: ' . print_r($_REQUEST, true), array(), WATCHDOG_DEBUG);
    }
    
    if ($uc_swipehq_test_mode) {
        drupal_set_message('Swipe Checkout: test mode enabled.', 'warning');
    }
    
    if (isset($_REQUEST['result']) && isset($_REQUEST['user_data']) && ctype_digit($_REQUEST['user_data'])) {
        $order_id = (int) $_REQUEST['user_data'];
        
        $order = uc_order_load($order_id);
        if ($order === FALSE) {
			return t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
        }
        
        if ($_REQUEST['result'] == 'accepted' || ($uc_swipehq_test_mode == 1 && $_REQUEST['result'] == 'test-accepted')) {
            //empty cart
            uc_cart_empty(uc_cart_get_id());
            
            drupal_set_message('Your payment was accepted. Thank you.');
            $message = variable_get('uc_swipehq_success_text', 'Thank you for your order.');
            
        } elseif ($_REQUEST['result'] == 'declined' || ($uc_swipehq_test_mode == 1 && $_REQUEST['result'] == 'test-declined')) {
            $message = 'Payment declined.';
        } else {
            $message = 'Error: undefined result of transaction.';
        }
    } elseif (isset($_REQUEST['status']) && isset($_REQUEST['td_user_data']) && ctype_digit($_REQUEST['td_user_data'])) {
        $order_id = $_REQUEST['td_user_data'];

		watchdog(SWIPE_WATCHDOG_NAME, 'Received LPN');
        
        //load order
        $order = uc_order_load($order_id);
        if ($order != FALSE && isset($_REQUEST['transaction_id'])) {
            if ($_REQUEST['status'] == 'test-accepted' || $_REQUEST['status'] == 'test-declined') {
                if ($uc_swipehq_test_mode == 1) {
                    switch ($_REQUEST['status']) {
                        case 'test-accepted':
                            $status =  variable_get('uc_swipehq_test_paid_status', 'payment_received');
                            uc_payment_enter($order_id, 'swipehq', $order->order_total, 0, NULL, t('TEST - Paid via Swipe Checkout'));

                            //add a comment to let sales team know this came in through the site.
                            uc_order_comment_save($order_id, 0, t('TEST - Order created through website.'), 'admin');
                            break;
                        case 'test-declined':
                            $status =  variable_get('uc_swipehq_test_failed_status', 'pending');
                            break;
                    }
                    //update status
                    $updated = uc_order_update_status($order_id, $status);
                    if ($updated === true && $uc_swipehq_debug) {
                        watchdog(SWIPE_WATCHDOG_NAME, 'Test mode enabled, status of order ID' . $order_id . ' has been updated to "' . $status . '"', array(), WATCHDOG_DEBUG);
                    }
                } else {
                    watchdog(SWIPE_WATCHDOG_NAME, 'Test mode not enabled, status of order ID' . $order_id . ' has not been updated.', array(), WATCHDOG_WARNING);
                }
            } elseif ($_REQUEST['status'] == 'accepted' || $_REQUEST['status'] == 'declined') {
                $is_verified = _uc_swipehq_verify_transaction($_REQUEST['transaction_id']);
                if ($is_verified) {
                    if ($uc_swipehq_debug) {
                        watchdog(SWIPE_WATCHDOG_NAME, 'Transaction ' . $_REQUEST['transaction_id'] . ' has been successfully verified.', array(), WATCHDOG_DEBUG);
                    }
                    
                    switch ($_REQUEST['status']) {
                        case 'accepted':
                            $status =  variable_get('uc_swipehq_paid_status', 'payment_received');
                            uc_payment_enter($order_id, 'swipehq', $order->order_total, 0, NULL, t('Paid via Swipe Checkout'));

                            //add a comment to let sales team know this came in through the site.
                            uc_order_comment_save($order_id, 0, t('Order created through website.'), 'admin');
                            break;
                        case 'declined':
                            $status =  variable_get('uc_swipehq_failed_status', 'pending');
                            break;
                    }
                    $updated = uc_order_update_status($order_id, $status);
                    
                    if ($uc_swipehq_debug) {
                        if ($updated === true) {
                            watchdog(SWIPE_WATCHDOG_NAME, 'Status of order ID' . $order_id . ' has been updated to "' . $status . '"', array(), WATCHDOG_DEBUG);
                        } else {
                            watchdog(SWIPE_WATCHDOG_NAME, 'Status of order ID' . $order_id . ' has not been updated.', array(), WATCHDOG_DEBUG);
                        }
                    }
                } else {
                    uc_order_comment_save($order_id, 0, t('Attempted unverified by Swipe Checkout completion for this order.'), 'admin');
                    watchdog(SWIPE_WATCHDOG_NAME, 'Verification of transaction ID ' . $request['transaction_id'] . ' has been failed, order ID: ' . $order_id, array(), WATCHDOG_DEBUG);
                }
            } else {
                watchdog(SWIPE_WATCHDOG_NAME, 'Undefined status of order ID' . $order_id . ' "%status"', array('%status' => $_REQUEST['status']), WATCHDOG_WARNING);
            }
        } else {
            watchdog(SWIPE_WATCHDOG_NAME, 'Undefined order ID' . $order_id, array(), WATCHDOG_ERROR);
        }
		watchdog(SWIPE_WATCHDOG_NAME, 'LPN done');
		die('LPN Done');
    } else {
        drupal_set_message('Sorry, not enough parameters to proceed your request.', 'error');
        watchdog(SWIPE_WATCHDOG_NAME, 'Not enough parameters to proceed request.', array(), WATCHDOG_NOTICE);
    }
    return t($message);
}
